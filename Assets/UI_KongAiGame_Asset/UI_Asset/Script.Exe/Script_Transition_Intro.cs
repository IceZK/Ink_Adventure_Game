using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_Transition_Intro : MonoBehaviour
{
    [SerializeField] public Animator transition;

    [SerializeField] public float transitionTime = 1f;

    // Team >> Game >> Warning >> Intro + Main Menu

    /*public void EndAnimationAndLoadNextScene()
    {
        // Load the next scene by name (change "NextSceneName" to the actual scene name)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1 );
    }*/

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))     //MOUSE LEFT
        {
            Debug.Log("Mouse Left Press");
           
        }
        LoadNextLevel();
        //EndAnimationAndLoadNextScene();

        /*if (Input.GetMouseButton(1))  //MOUSE RIGHT
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (Input.GetMouseButton(2))    //MOUSE MID(SCROLL)
        {
            SceneManager.LoadScene("Scene_LogoTeam.Exe");
        }*/
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        Debug.Log("Go to NS.");
        transition.SetTrigger("Trigger");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelIndex);
    }


}
