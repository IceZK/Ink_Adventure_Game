using UnityEngine;
using UnityEngine.SceneManagement;

public class EndAnimation_GoNextScene : MonoBehaviour
{
    public Animator animator; // Reference to the Animator component controlling the animation
    public string nextSceneName; // The name of the next scene you want to load

    private bool animationEnded = false;

    private void Start()
    {
        // Subscribe to the animation event when it finishes
        AnimationEvent animationEvent = new AnimationEvent
        {
            time = animator.GetCurrentAnimatorClipInfo(0).Length,
            functionName = "OnAnimationEnd"
        };
        animator.runtimeAnimatorController.animationClips[0].AddEvent(animationEvent);
    }

    // Called when the animation ends
    private void OnAnimationEnd()
    {
        animationEnded = true;
    }

    private void Update()
    {
        // Check if the animation has ended and the user presses a key or clicks the mouse
        if (animationEnded && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
        {
            LoadNextScene();
        }
    }

    // Function to load the next scene
    private void LoadNextScene()
    {
        SceneManager.LoadScene(nextSceneName);
    }
}