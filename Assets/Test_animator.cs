using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_animator : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator animator;
    public Player_SFX player_SFX;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void idle()
    {
        animator.SetBool("isIdle", true);
        animator.SetBool("isWalk", false);
        player_SFX.Stoploop();
    }
    public void Walk()
    {
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalk", true);
        
    }
    public void Attack()
    {
        animator.SetBool("isIdle", false);
        animator.SetBool("isAttack", true);
    }
    
}
