using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Kagerou
{
    public class CombatUI : MonoBehaviour
    {
        public TMP_Text player_status;
        public Slider m_player_slider;
        public TMP_Text game_status;
        public TMP_Text enemy_status;
        public Slider m_enemy_slider;

        public Image Attack;
        public Image Defense;

        private float player_baseMaxInk;
        private float player_baseInk;
        private float player_baseDamage;
        private float enemy_baseMaxInk;
        private float enemy_baseInk;
        private float enemy_baseDamage;

        public FightingSystem fighting;
        public bool isInCombat;
        public bool isEscaped = false;
        
        //UI
        public bool CombatMenu = false;
        public bool EscapeMenu = false;
        public GameObject Combat_Canvas;
        public GameObject Escape_Canvas;

        public Button A1;
        public Button A2;
        public Button A3;
        public Button A4;
        // Start is called before the first frame update
        void Start()
        {

            fighting = GetComponent<FightingSystem>();
            
        }

        // Update is called once per frame
        void Update()
        {
            //fighting = FindObjectOfType<FightingSystem>();
            //GameObject Fighto = GameObject.Find("Dokkan battle");
            //fighting = Fighto.GetComponent<FightingSystem>();


            //issu
            if (Character_Status_Manager.isCombat == true)
            {
                Time.timeScale = 0;
                Combat_Canvas.SetActive(true);
                isInCombat = true;


            }
            else if (Character_Status_Manager.isCombat == false && isInCombat == true)
            {

                Time.timeScale = 1;
                Combat_Canvas.SetActive(false);

            }

            player_baseMaxInk = fighting.player_baseMaxInk;
            player_baseInk = fighting.player_baseInk;
            player_baseDamage = fighting.player_baseDamage;
            enemy_baseMaxInk = fighting.enemy_baseMaxInk;
            enemy_baseInk = fighting.enemy_baseInk;
            enemy_baseDamage = fighting.enemy_baseDamage;

            m_player_slider.value = player_baseInk;
            m_enemy_slider.value = enemy_baseInk;
            player_status.text = "HP: " + (int)player_baseInk + " / " + player_baseMaxInk + "\n Atk: " + player_baseDamage;
            game_status.text = FightingSystem.phase_text;
            enemy_status.text = "HP: " + enemy_baseInk + " / " + enemy_baseMaxInk + "\n Atk: " + enemy_baseDamage;

            //opne Combat

            //Escape menu
            if(Input.GetKeyDown(KeyCode.Q) && EscapeMenu == false) 
            {
                Escape();
            }
            if(Input.GetKeyDown(KeyCode.Y) && EscapeMenu == true)
            {
                EscapeYes();
            }
            if(Input.GetKeyDown(KeyCode.N) && EscapeMenu == true)
            {
                EscapeNo();
            }
            
        }
        public void Escape()
        {
            Escape_Canvas.SetActive(true);
            EscapeMenu = true;
            A1.interactable = false;
            A2.interactable = false;
            A3.interactable = false;
            A4.interactable = false;
        }
        public void EscapeYes()
        {
            Character_Status_Manager.player_baseInk -= (Character_Status_Manager.player_baseMaxInk * 0.1f);
            isEscaped = true;
            isInCombat = false;
            CombatMenu = false;
            EscapeMenu = false;
            Combat_Canvas.SetActive(false);
            Escape_Canvas.SetActive(false);
            Time.timeScale = 1;
            A1.interactable = true;
            A2.interactable = true;
            A3.interactable = true;
            A4.interactable = true;
        }

        public void EscapeNo()
        {
            Escape_Canvas.SetActive(false);
            EscapeMenu = false;
            A1.interactable = true;
            A2.interactable = true;
            A3.interactable = true;
            A4.interactable = true;
        }



    }
}
