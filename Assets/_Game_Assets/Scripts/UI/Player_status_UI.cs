﻿using IceZK_DEV;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Game_Assets.Code.UI
{
    public class Player_status_UI : MonoBehaviour
    {
        public TextMeshProUGUI text_atk;
        public TextMeshProUGUI text_ink;
        public TextMeshProUGUI text_inktemp;
        //public TextMeshProUGUI text_Weapon;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            text_atk.text = "ATK: " + Character_Status_Manager.player_baseDamage;
            text_ink.text = "Ink: " + Character_Status_Manager.player_baseInk + "/" + Character_Status_Manager.player_baseMaxInk;
            text_inktemp.text = "Ink Temp: " + Character_Status_Manager.player_InkTemp;
            
        }
    }
}