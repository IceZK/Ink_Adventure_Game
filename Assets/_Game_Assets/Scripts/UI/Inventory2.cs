﻿using Assets.Game_Assets.Code.Singleton;
using IceZK_DEV;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Game_Assets.Code.UI
{
    public class Inventory2 : MonoBehaviour
    {
        public GameObject InventoryMenu;
        private bool MenuActivated;
        public TextMeshProUGUI m_status;
        public TextMeshProUGUI m_wood;
        public TextMeshProUGUI m_stone;
        public TextMeshProUGUI m_mon;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //open inventory
            if (Input.GetButtonDown("Inventory") && MenuActivated)
            {
                CloseInventory();


            }
            else if (Input.GetButtonDown("Inventory") && !MenuActivated)
            {
                openInventory();


            }
            //show info-------------------------------------------------------------------------------------------------------
            m_status.text =   "LV: " + Character_Status_Manager.player_LV
                            + "\nEXP: " + Character_Status_Manager.player_EXP
                            + "\nInk: " + (int)Character_Status_Manager.player_baseInk + "/" + Character_Status_Manager.player_baseMaxInk
                            + "\nATK: " + Character_Status_Manager.player_baseDamage
                            + "\nCurse: " + GameManager.curse
                            + "\nDrainCurse: " + (1+(GameManager.curse * 0.1)) + "/s"
                            ;

            m_wood.text = "Wood: " + ResourceMannage.i_wood + "/" + Character_Status_Manager.player_baseBagCap;
            m_stone.text = "Stone: " + ResourceMannage.i_stone + "/" + Character_Status_Manager.player_baseBagCap;
            m_mon.text = "MonMaterial: " + ResourceMannage.i_monMat + "/" + Character_Status_Manager.player_baseBagCap;
        }
        public void openInventory()
        {
            Time.timeScale = 0;
            InventoryMenu.SetActive(true);
            MenuActivated = true;
        }
        public void CloseInventory()
        {
            Time.timeScale = 1;
            InventoryMenu.SetActive(false);
            MenuActivated = false;
        }
    }
}