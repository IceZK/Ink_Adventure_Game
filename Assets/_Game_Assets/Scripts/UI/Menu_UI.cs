using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu_UI : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Menu;
    public bool MenuActivated = false;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Menu") && MenuActivated)
        {
            CloseMenu();

        }
        else if (Input.GetButtonDown("Menu") && !MenuActivated)
        {

            OpenMenu();
        }
    }
    public void CloseMenu()
    {
        Time.timeScale = 1;
        Menu.SetActive(false);
        MenuActivated = false;
    }
    public void OpenMenu()
    {
        Time.timeScale = 0;
        Menu.SetActive(true);
        MenuActivated = true;
    }
}
