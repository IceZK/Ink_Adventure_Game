using IceZK_DEV;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Kagerou
{
    public class FightingSystem : MonoBehaviour
    {
        //public CombatUI m_combatui;
        public Player_SFX m_SFX;

        [SerializeField] protected int phase = 0;
        [SerializeField] protected int playerAction = 0;
        [SerializeField] protected int enemyAction = 0;
        [SerializeField] protected float timer = 30;
        [SerializeField] protected float counter = 0;
        private bool isRandom = false;
        public static string phase_text;
        public GameObject Attack;
        public GameObject Defense;
        public Button action_1;
        public Button action_2;
        public Button action_3;
        public Button action_4;

        public float player_baseMaxInk;
        public float player_baseInk;
        public int player_baseDamage;
        public int player_EXP;
        public int player_monMat;
        public float enemy_baseMaxInk;
        public float enemy_baseInk;
        public int enemy_baseDamage;
        public int enemy_EXP;
        public int enemy_monMat;

        //player Animation
        public GameObject player_Idle;
        public GameObject player_Attack_1;
        public GameObject player_Attack_2;
        public GameObject player_Defense;
        public GameObject player_Hurt;

        //enemy Animation
        public GameObject enemy_Idle;
        public GameObject enemy_Attack_1;
        public GameObject enemy_Attack_2;
        public GameObject enemy_Defense;
        public GameObject enemy_Hurt;

        // Start is called before the first frame update
        void Start()
        {
            
            
            enemy_baseMaxInk = Character_Status_Manager.enemy_baseMaxInk;
            enemy_baseInk = Character_Status_Manager.enemy_baseInk;
            enemy_baseDamage = Character_Status_Manager.enemy_baseDamage;
            enemy_EXP = Character_Status_Manager.enemy_EXP;
            enemy_monMat = Character_Status_Manager.enemy_monMat;

            Player_SFX m_SFX = GameObject.Find("Player").GetComponent<Player_SFX>();


            
        }

        // Update is called once per frame
        void Update()
        {
            player_baseMaxInk = Character_Status_Manager.player_baseMaxInk;
            player_baseInk = Character_Status_Manager.player_baseInk;
            player_baseDamage = Character_Status_Manager.player_baseDamage;
            player_EXP = Character_Status_Manager.player_EXP;
            player_monMat = ResourceMannage.i_monMat;
            //Debug.Log(playerAction);
            //Debug.Log(enemyAction);
            if(phase == 0 && !isRandom) 
            {
                enemyAction = Random.Range(1, 4);
                isRandom = true;
            }
            if(phase == 2 && !isRandom) 
            {
                enemyAction = Random.Range(1, 4);
                isRandom = true;
            }
            ActionPhase();
            WinLoseCondition();
        }
        public void OnAction1Click()
        {
            playerAction = 1;
        }
        public void OnAction2Click()
        {
            playerAction = 2;
        }
        public void OnAction3Click()
        {
            playerAction = 3;
        }
        public void OnAction4Click()
        {
            playerAction = 4;
        }
     
        private void ActionPhase()
        {
            switch(phase) 
            {
                case 0:AttackSelectPhase();
                    break;
                case 1:AttackAnimatePhase();
                    break;
                case 2:DefenseSelectPhase();
                    break;
                case 3:DefenseAnimatePhase();
                    break;
            }
        }
        private void AttackSelectPhase()
        {
            //player Animation set
            player_Idle.SetActive(true);
            player_Attack_1.SetActive(false);
            player_Attack_2.SetActive(false);
            player_Defense.SetActive(false);
            player_Hurt.SetActive(false);
            //enemy Animation set
            enemy_Idle.SetActive(true);
            enemy_Attack_1.SetActive(false);
            enemy_Attack_2.SetActive(false);
            enemy_Defense.SetActive(false);
            enemy_Hurt.SetActive(false);
            if(playerAction > 0)
            {
                counter++;
                Debug.Log(timer + " " + counter);
                if(counter>=timer) 
                {
                    phase++;
                    //player Animation set
                    player_Idle.SetActive(false);
                    player_Attack_1.SetActive(true);
                    player_Attack_2.SetActive(false);
                    player_Defense.SetActive(false);
                    player_Hurt.SetActive(false);
                    //enemy Animation set
                    enemy_Idle.SetActive(true);
                    enemy_Attack_1.SetActive(false);
                    enemy_Attack_2.SetActive(false);
                    enemy_Defense.SetActive(false);
                    enemy_Hurt.SetActive(false);
                    counter = 0;
                }
            }
            phase_text = "Attack Select Phase";
            Defense.active = false;
            Attack.active = true;
        }
        private void AttackAnimatePhase()
        {
            if (enemyAction == playerAction)
            {
                enemyAction = 0;
                playerAction = 0;
                //player Animation set
                player_Idle.SetActive(false);
                player_Attack_1.SetActive(false);
                player_Attack_2.SetActive(true);
                player_Defense.SetActive(false);
                player_Hurt.SetActive(false);
                //enemy Animation set
                enemy_Idle.SetActive(false);
                enemy_Attack_1.SetActive(false);
                enemy_Attack_2.SetActive(false);
                enemy_Defense.SetActive(true);
                enemy_Hurt.SetActive(false);
            }
            if (enemyAction != playerAction)
            {
                enemy_baseInk -= player_baseDamage;
                //sound
                m_SFX.AttackSFX();
                enemyAction = 0;
                playerAction = 0;
                //player Animation set
                player_Idle.SetActive(false);
                player_Attack_1.SetActive(false);
                player_Attack_2.SetActive(true);
                player_Defense.SetActive(false);
                player_Hurt.SetActive(false);
                //enemy Animation set
                enemy_Idle.SetActive(false);
                enemy_Attack_1.SetActive(false);
                enemy_Attack_2.SetActive(false);
                enemy_Defense.SetActive(false);
                enemy_Hurt.SetActive(true);
            }
            counter ++;
            Debug.Log(timer + " " + counter);
            if (counter >= timer)
            {
                phase++;
                isRandom = false;
                phase_text = "Attack Animate Phase";
                Defense.active = false;
                Attack.active = true;
                counter = 0;

            }
        }
        private void DefenseSelectPhase()
        {
            //player Animation set
            player_Idle.SetActive(true);
            player_Attack_1.SetActive(false);
            player_Attack_2.SetActive(false);
            player_Defense.SetActive(false);
            player_Hurt.SetActive(false);
            //enemy Animation set
            enemy_Idle.SetActive(true);
            enemy_Attack_1.SetActive(false);
            enemy_Attack_2.SetActive(false);
            enemy_Defense.SetActive(false);
            enemy_Hurt.SetActive(false);
            if (playerAction > 0)
            {
                counter ++;
                Debug.Log(timer + " " + counter);
                if (counter >= timer)
                {
                    phase++;
                    //player Animation set
                    player_Idle.SetActive(true);
                    player_Attack_1.SetActive(false);
                    player_Attack_2.SetActive(false);
                    player_Defense.SetActive(false);
                    player_Hurt.SetActive(false);
                    //enemy Animation set
                    enemy_Idle.SetActive(false);
                    enemy_Attack_1.SetActive(true);
                    enemy_Attack_2.SetActive(false);
                    enemy_Defense.SetActive(false);
                    enemy_Hurt.SetActive(false);
                    counter = 0;
                }
            }
            phase_text = "Defense Select Phase";
            Defense.active = true;
            Attack.active = false;
        }
        private void DefenseAnimatePhase()
        {
            if (enemyAction == playerAction)
            {
                enemyAction = 0;
                playerAction = 0;
                //player Animation set
                player_Idle.SetActive(false);
                player_Attack_1.SetActive(false);
                player_Attack_2.SetActive(false);
                player_Defense.SetActive(true);
                player_Hurt.SetActive(false);
                //enemy Animation set
                enemy_Idle.SetActive(false);
                enemy_Attack_1.SetActive(false);
                enemy_Attack_2.SetActive(true);
                enemy_Defense.SetActive(false);
                enemy_Hurt.SetActive(false);
            }
            if (enemyAction != playerAction)
            {
                Character_Status_Manager.player_baseInk -= enemy_baseDamage;
                //Sound
                m_SFX.DamageSFX();
                enemyAction = 0;
                playerAction = 0;
                //player Animation set
                player_Idle.SetActive(false);
                player_Attack_1.SetActive(false);
                player_Attack_2.SetActive(false);
                player_Defense.SetActive(false);
                player_Hurt.SetActive(true);
                //enemy Animation set
                enemy_Idle.SetActive(false);
                enemy_Attack_1.SetActive(false);
                enemy_Attack_2.SetActive(true);
                enemy_Defense.SetActive(false);
                enemy_Hurt.SetActive(false);
            }
            counter ++;
            Debug.Log(timer + " " + counter);
            if (counter >= timer)
            {
                phase = 0;
                isRandom = false;
                phase_text = "Defense Animate Phase";
                Defense.active = true;
                Attack.active = false;
                counter = 0;
            }
        }

        //bool isCoroutineRunning = false;
        private void WinLoseCondition()
        {

            if (enemy_baseInk <= 0)
            {
                Debug.Log("You Win");
                m_SFX.WinSFX();
                player_EXP += enemy_EXP;
                Character_Status_Manager.player_EXP = player_EXP;
                Debug.Log($"Add {enemy_EXP} EXP to Player");
                player_monMat += enemy_monMat;
                ResourceMannage.i_monMat = player_monMat;
                Debug.Log($"Add {enemy_monMat} Material to Player");
                Character_Status_Manager.player_baseInk += enemy_baseMaxInk;

                //SceneManager.UnloadScene("Combat");
                Character_Status_Manager.isCombat = false;
                //qte.qte();
                /*if (isCoroutineRunning) return;
                isCoroutineRunning = true;
                StartCoroutine(QTECombat());*/
            }
            else if (player_baseInk <= 0)
            {
                Debug.Log("You Lose");
                m_SFX.loseSFX();
                //SceneManager.UnloadScene("Combat");
                Character_Status_Manager.isCombat = false;

            }
        }

        /*IEnumerator QTECombat()
        {

            float time = 100;
            int totalframe = (int)time * 60;
            int round = 0;
            char[] QTEArray = { 'Z', 'X', 'C' };
            char selectQTE;
            int index;
            index = Random.Range(0, QTEArray.Length);
            selectQTE = QTEArray[index];
            Debug.Log($"Press {selectQTE.ToString()}");
            switch (index)
            {
                case 0:
                    {
                        if (Input.anyKeyDown)
                        {
                            if (Input.GetKeyDown(KeyCode.Z))
                            {
                                totalframe = (int)time * 60;
                                round++;
                                //isRandom = false;
                                OnQTESuccess();
                                yield break;
                            }
                            else
                            {
                                Debug.Log("QTE Failed Not Z");
                                OnQTEFailure();
                                yield break;
                            }
                        }
                    }
                    break;
                case 1:
                    {
                        if (Input.anyKeyDown)
                        {
                            if (Input.GetKeyDown(KeyCode.X))
                            {
                                totalframe = (int)time * 60;
                                round++;
                                //isRandom = false;
                                OnQTESuccess();
                                yield break;
                            }
                            else
                            {
                                Debug.Log("QTE Failed Not X");
                                OnQTEFailure();
                                yield break;
                            }

                        }
                    }
                    break;
                case 2:
                    {
                        if (Input.anyKeyDown)
                        {
                            if (Input.GetKeyDown(KeyCode.C))
                            {
                                totalframe = (int)time * 60;
                                round++;
                                //isRandom = false;
                                OnQTESuccess();
                                yield break;
                            }
                            else
                            {
                                Debug.Log("QTE Failed Not C");
                                OnQTEFailure();
                                yield break;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
                    *//*bool isRandom = false;
                    if (!isRandom)
                    {
                        index = Random.Range(0, QTEArray.Length);
                        selectQTE = QTEArray[index];
                        Debug.Log($"Press {selectQTE.ToString()}");
                        isRandom = true;
                        switch (index)
                        {
                            case 0:
                                {
                                    if (Input.anyKeyDown)
                                    {
                                        if (Input.GetKeyDown(KeyCode.Z))
                                        {
                                            totalframe = (int)time * 60;
                                            round++;
                                            isRandom = false;
                                            OnQTESuccess();
                                            yield break;
                                        }
                                        else
                                        {
                                            Debug.Log("QTE Failed Not Z");
                                            OnQTEFailure();
                                            yield break;
                                        }
                                    }
                                }
                                break;
                            case 1:
                                {
                                    if (Input.anyKeyDown)
                                    {
                                        if (Input.GetKeyDown(KeyCode.X))
                                        {
                                            totalframe = (int)time * 60;
                                            round++;
                                            isRandom = false;
                                            OnQTESuccess();
                                            yield break;
                                        }
                                        else
                                        {
                                            Debug.Log("QTE Failed Not X");
                                            OnQTEFailure();
                                            yield break;
                                        }

                                    }
                                }
                                break;
                            case 2:
                                {
                                    if (Input.anyKeyDown)
                                    {
                                        if (Input.GetKeyDown(KeyCode.C))
                                        {
                                            totalframe = (int)time * 60;
                                            round++;
                                            isRandom = false;
                                            OnQTESuccess();
                                            yield break;
                                        }
                                        else
                                        {
                                            Debug.Log("QTE Failed Not C");
                                            OnQTEFailure();
                                            yield break;
                                        }
                                    }
                                }
                                break;
                            default: break;
                        }
                    }*//*
                    do { totalframe -= 1; yield return null; } while (totalframe > 0);
            if (totalframe <= 0 && round < 3)
            {
                Debug.Log("QTE Failed Time Out");
            }
        }

        private void OnQTESuccess()
        {
            Debug.Log("Success!!");
            isCoroutineRunning = false;
            Character_Status_Manager.isCombat = false;
        }
        private void OnQTEFailure()
        {
            isCoroutineRunning = false;
            Character_Status_Manager.isCombat = false;
        }*/


        public void ResetFight()
        {
            enemy_baseInk = 1;
            
        }

    }
}
