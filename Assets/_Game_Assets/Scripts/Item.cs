﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IceZK_DEV
{
    public class Item : MonoBehaviour
    {
        //ดึงค่าจากSO
        public ItemSO m_ItemSO;
        //ดึงค่าenum
        protected ItemType m_ItemType;
        protected MatType m_matType;
        public string ItemName;
        public Sprite icon;
        public ItemType type;
        public MatType matType;
        public int amount;
        [Header("Only Huge Material")]
        public int HugeAmount;



        // Update is called once per frame

        private void Start()
        {
            ItemName = m_ItemSO.ItemName;
            icon = m_ItemSO.icon;
            type = m_ItemSO.type;
            matType = m_ItemSO.mattype;
            amount = m_ItemSO.amount;
            this.gameObject.name = m_ItemSO.ItemName;
        }
        void update()
        {
            
        }
        public Item(string newItemName, ItemType newtype, Sprite newicon, int newamount)
        {
            ItemName = newItemName;
            type = newtype;
            amount = newamount;
            icon = newicon;
        }
    }
}

