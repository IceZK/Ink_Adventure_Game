using Assets.Game_Assets.Code.Singleton;
using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusic : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource m_AudioSource;
    public AudioSource m_AudioSource2;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        
        
      
        if (Character_Status_Manager.isCombat == true)
        {
            CombatMusic();
        }
        else if (Character_Status_Manager.isCombat == false)
        {
            GameplayMusic();
        }




    }
    public void GameplayMusic()
    {

        m_AudioSource.enabled = true;
        m_AudioSource2.enabled = false;
        
    }
    public void CombatMusic()
    {

        m_AudioSource.enabled = false;
        m_AudioSource2.enabled = true;
    }
}
