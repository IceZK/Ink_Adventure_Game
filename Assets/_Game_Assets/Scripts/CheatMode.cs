using Assets.Game_Assets.Code.Singleton;
using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatMode : MonoBehaviour
{
    // Start is called before the first frame update
    public static CheatMode CM_instance;

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (CM_instance != null && CM_instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            CM_instance = this;


        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F3)) 
        {
            Character_Status_Manager.player_baseBagCap = 9999;
            
            Character_Status_Manager.player_EXP = 100000000;
            Character_Status_Manager.player_baseCurse = 9999;
            

            ResourceMannage.i_wood = 9999;
            ResourceMannage.i_stone = 9999;
            ResourceMannage.i_monMat = 9999;

            ResourceMannage.b_wood = 9999;
            ResourceMannage.b_stone = 9999;
            ResourceMannage.b_monMat = 9999;
            ResourceMannage.ink = 9999;

        }
        if(Input.GetKeyDown(KeyCode.F5))
        {
            GameManager.curse = 100;
        }
    }
}
