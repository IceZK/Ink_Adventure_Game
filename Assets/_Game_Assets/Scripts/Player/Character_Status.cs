using Assets.Game_Assets.Code.Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IceZK_DEV
{
    public class Character_Status : MonoBehaviour
    {
        //public Character_Stat_OS StatOS;
        

        public string charName = "name";
        //base stat
        [Header("base stat")]
        
        public float MaxCurse = 0;
        public float MaxInk = 0;
        public int BagCap = 0;

        
        public int CurrentDamage = 0;
        
        public float CurrentInk = 0;
        public float player_InkTemp = 0;
        public int CurrentBag = 0;

        public int EXP = 0;
        public int LV = 0;

        public float CurseTime = 1;
        

        //Modifier stat
        /*[Header("Modifier stat")]
        
        public float ModMaxCurse = 0;
        public float ModMaxInk = 0;
        public int ModDamage = 0;
        public int ModBag = 0;
        */
        void Start()
        {
           // charName = StatOS.charName;
            
            MaxCurse = Character_Status_Manager.player_baseMaxCurse;
            MaxInk = Character_Status_Manager.player_baseMaxInk;
            BagCap = Character_Status_Manager.player_baseBagCap;

           
            CurrentDamage = Character_Status_Manager.player_baseDamage;
            
            
            CurrentBag = Character_Status_Manager.player_baseBag;

            /*
            ModMaxCurse = StatOS.MaxCurse;
            ModMaxInk = StatOS.MaxInk;
            ModDamage = StatOS.Damage;
            ModBag = StatOS.Bag;
            */
            EXP = Character_Status_Manager.player_EXP;
            LV = Character_Status_Manager.player_LV;
        }
        void Update()
        {
            /*
            MaxCurse += ModMaxCurse;
            MaxInk += ModMaxInk;
            CurrentDamage += ModDamage;
            BagCap += ModBag;
            */
            //Debug.Log(CurseTime);
            CurseTime -= Time.deltaTime;
            CurrentInk = Character_Status_Manager.player_baseInk;
            if(Character_Status_Manager.player_baseInk > Character_Status_Manager.player_baseMaxInk)
            {
                Character_Status_Manager.player_InkTemp += (Character_Status_Manager.player_baseInk - Character_Status_Manager.player_baseMaxInk);
                Character_Status_Manager.player_baseInk -= Character_Status_Manager.player_baseInk - Character_Status_Manager.player_baseMaxInk;
            }
            IsPlayerAlive();


            //lost ink per curse
            
            if(CurseTime <= 0)
            {
                Character_Status_Manager.player_baseInk -= 1 + (GameManager.curse * 0.1f);
                CurseTime = 1;
            }
            
        }
        public void SaveMat()
        {
            ResourceMannage.ink += Character_Status_Manager.player_InkTemp;
            

            ResourceMannage.b_wood += ResourceMannage.i_wood;
            ResourceMannage.b_stone += ResourceMannage.i_stone;
            ResourceMannage.b_monMat += ResourceMannage.i_monMat;
            
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name == "Door")
            {
                SaveMat();
                ResourceMannage.i_wood = 0;
                ResourceMannage.i_stone = 0;
                ResourceMannage.i_monMat = 0;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            
        }

        public void IsPlayerAlive()
        {
            if(Character_Status_Manager.player_baseInk <= 0)
            {
                Character_Status_Manager.isCombat = false;
                ResourceMannage.i_wood = 0;
                ResourceMannage.i_stone = 0;
                ResourceMannage.i_monMat = 0;
                
                SceneManager.LoadScene("Town");

            }
        }

    }
}

