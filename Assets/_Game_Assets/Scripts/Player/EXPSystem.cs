using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kagerou
{
    public class EXPSystem : MonoBehaviour
    {
        [SerializeField] private int player_EXP;
        [SerializeField] private int player_LV;
        [SerializeField] private int enemy_EXP;

        protected int[] exp_Level = 
            {
            100,
            150,
            225,
            337,
            505,
            757,
            1135,
            1702,
            2553,
            3829,
            5743,
            8614,
            12921,
            19381,
            29071,
            43606,
            65409,
            98113,
            147169,
            220753,
            331129,
            496693,
            745039,
            1117558,
            1676337,
            2514505,
            3771757,
            5657635,
            8486452,
            12729678
            };
        [SerializeField] private int targetEXP;

        // Start is called before the first frame update
        void Start()
        {
            player_EXP = Character_Status_Manager.player_EXP;
            player_LV = Character_Status_Manager.player_LV;
            enemy_EXP = Character_Status_Manager.enemy_EXP;
            targetEXP = exp_Level[player_LV];
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void levelUp()
        {
            targetEXP = exp_Level[player_LV];
            if(player_EXP > 0 && player_EXP >= targetEXP)
            do
            {
                player_EXP -= targetEXP;
                player_LV++;
                Character_Status_Manager.player_LV = player_LV;
                Character_Status_Manager.player_baseDamage += 5;
                Character_Status_Manager.player_baseMaxInk += 50;
                if (player_LV < 30)
                    targetEXP = exp_Level[player_LV];
            } while (player_EXP >= targetEXP && player_LV < 30);
            
        }
    }
}