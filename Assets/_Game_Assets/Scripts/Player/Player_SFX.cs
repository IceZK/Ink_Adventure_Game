using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_SFX : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource m_SFX;
    public AudioClip SFX_Walk, SFX_Pick, SFX_Attack, SFX_Damage, SFX_Win, SFX_Lose;
   
    public void WalkSFX()
    {
        m_SFX.clip = SFX_Walk;
        
        m_SFX.Play();
        
    }
    public void Stoploop()
    {
        m_SFX.loop = false;
    }
    public void PickupSFX()
    {
        m_SFX.clip = SFX_Pick;
        m_SFX.Play();
    }
    public void AttackSFX()
    {
        m_SFX.clip = SFX_Attack;
        m_SFX.Play();
    }
    public void DamageSFX()
    {
        m_SFX.clip = SFX_Damage;
        m_SFX.Play();
    }
    public void WinSFX()
    {
        m_SFX.clip = SFX_Win;
        m_SFX.Play();
    }
    public void loseSFX()
    {
        m_SFX.clip = SFX_Lose;
        m_SFX.Play();
    }
}
