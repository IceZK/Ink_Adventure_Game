﻿using System.Collections;
using TMPro;
using UnityEngine;



namespace IceZK_DEV
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        //private InventoryManager inventoryManager;
        private float loottime;
        bool check;
        public int HugeAmount;
        public TextMeshProUGUI Player_Text;
        public GameObject Text;
        float counter = 2;
        //get component
        public Player_SFX m_player_SFX;
        void Start()
        {
            //inventoryManager = GameObject.Find("InventoryCanvas").GetComponent<InventoryManager>();
            m_player_SFX = GetComponent<Player_SFX>();


        }
        public void Update()
        {
            loottime -= Time.deltaTime;
            counter -= Time.deltaTime;
            

            // Resourse == BagCap
            if(ResourceMannage.i_wood >= Character_Status_Manager.player_baseBagCap)
            {
                ResourceMannage.i_wood = Character_Status_Manager.player_baseBagCap;
            }
            if (ResourceMannage.i_stone >= Character_Status_Manager.player_baseBagCap)
            {
                ResourceMannage.i_stone = Character_Status_Manager.player_baseBagCap;
            }
            if (ResourceMannage.i_monMat >= Character_Status_Manager.player_baseBagCap)
            {
                ResourceMannage.i_monMat = Character_Status_Manager.player_baseBagCap;
            }

            if(Text.active == true)
            {
                if(counter <= 0)
                {
                    Text.active = false;
                }
            }
        }
        private void OnTriggerEnter(Collider other)
        {   
            Item item = other.GetComponent <Item>();

            //var inventory = GetComponent<InventoryManager>();
            

            if (item != null)
            {
                
                switch (item.type)
                {
                   
                    case ItemType.Material:
                        ResourceMannage.Resouurce_instance.AddItem(item.matType,item.amount);
                        TextGetItem(item.ItemName, item.amount);
                        //CloseText();
                        Destroy(other.gameObject, 0);
                        break;
                    case ItemType.HugeMaterial:
                        //get object value HugeAmount
                        HugeAmount = item.HugeAmount;
                        //code
                        if (Input.GetKeyDown("Loot") && check == false)
                        {
                            loottime = 3;
                            check = true;
                        }
                        else if (Input.GetKeyDown("Loot") && loottime <= 0 && check == true && HugeAmount >= 0)
                        {
                            ResourceMannage.Resouurce_instance.AddItem(item.matType, item.amount);
                            TextGetItem(item.name, item.amount);
                            //CloseText();
                            HugeAmount -= item.amount;
                            loottime = 3;
                        }
                        else if (Input.GetKeyDown("Loot") && loottime <= 0 && check == true && HugeAmount <= 0)
                        {
                            ResourceMannage.Resouurce_instance.AddItem(item.matType, item.amount);
                            TextGetItem(item.name, item.amount);
                            //CloseText();
                            HugeAmount -= item.amount;
                            Destroy(other.gameObject, 0);
                            check = false;
                        }
                        
                        break;

                }
                
                
            }
            




        }
        public void TextGetItem(string name, int amount)
        {
            m_player_SFX.PickupSFX();
            Text.active = true;
            Player_Text.text = "+ " + " " + amount + " " + name;
            counter = 2;

        }

        
    }
}