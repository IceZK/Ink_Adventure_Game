using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Assets.Game_Assets.Code.Singleton;
using UnityEngine.UI;

public class Show_UI_Resource_Version2 : MonoBehaviour
{
    // Start is called before the first frame update
    //UI
    public TextMeshProUGUI m_text_Resourse;
    public TextMeshProUGUI m_text_Day;
    public TextMeshProUGUI m_text_Curse;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        m_text_Resourse.text = ": " + ResourceMannage.b_wood + "\n: " + ResourceMannage.b_stone + "\n: " + ResourceMannage.b_monMat + "\n: " + ResourceMannage.ink;

        m_text_Day.text = "Day: " + GameManager.day;

        m_text_Curse.text = "Curse: " + GameManager.curse;
    }
}

