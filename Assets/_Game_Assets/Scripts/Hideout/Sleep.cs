using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IceZK_DEV;
using Assets.Game_Assets.Code.Singleton;
using UnityEngine.SceneManagement;

namespace Kagerou
{
    public class Sleep : MonoBehaviour
    {
        public GameObject BG_Day;
        public GameObject BG_Night;
        public GameObject SUN;
        public GameObject Moon;
        // Start is called before the first frame update

        public void Update()
        {
            if(GameManager.isSleep==false)
            {
                //Sun-Moon Icon
                SUN.SetActive(true);
                Moon.SetActive(false);

                //Day-Night BG
                BG_Day.SetActive(true);
                BG_Night.SetActive(false);

            }
            if (GameManager.isSleep == true)
            {
                //Sun-Moon Icon
                SUN.SetActive(false);
                Moon.SetActive(true);

                //Day-Night BG
                BG_Day.SetActive(false);
                BG_Night.SetActive(true);
            }
        }
        public void Goout()
        {
            if (GameManager.isSleep == false)
            {
                //fill ink with ink tank
                ResourceMannage.ink -= (Character_Status_Manager.player_baseMaxInk - Character_Status_Manager.player_baseInk);
                Character_Status_Manager.player_baseInk += (Character_Status_Manager.player_baseMaxInk - Character_Status_Manager.player_baseInk);
                //reset ink temp
                Character_Status_Manager.player_InkTemp = 0;
                GameManager.isSleep = true;
                GameManager.curse++;
                SceneManager.LoadScene("Gameplay");
                

            }
        }
        public void SleepNextDay()
        {
            Debug.Log(GameManager.isSleep);
            Debug.Log(GameManager.day);
            if (GameManager.isSleep == true)
            {
                GameManager.day++;
                GameManager.isSleep = false;
            }
        }
    }
}