using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Hover : MonoBehaviour
{
    // Start is called before the first frame update
    public Button m_button;
    public Image m_image;
    public Sprite Hover_sprite;
    public Sprite Left_sprite;
    void Start()
    {
        m_button = GetComponent<Button>();
        m_image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MouseHover()
    {
        m_image.sprite = Hover_sprite;
    }
    public void LeftMouse()
    {
        m_image.sprite = Left_sprite;
    }
}
