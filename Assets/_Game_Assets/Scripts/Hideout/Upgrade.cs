using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    // Start is called before the first frame update
    //cost
    
    public int[,] AtkCost = new int[4,3]
    { { 10, 20, 30 }, { 10, 20, 30 }, { 10, 20, 30, }, { 10, 20, 30, } };

    public int[,] HpCost = new int[4, 3] 
    { { 10, 20, 30 }, { 10, 20, 30 }, { 10, 20, 30, }, { 10, 20, 30, } };
    
    public int[,] BagCapCost = new int[4, 3] 
    { { 10, 20, 30 }, { 10, 20, 30 }, { 10, 20, 30, }, { 10, 20, 30, } };
    
    public int[,] TankCost = new int[4, 3] 
    { { 10, 20, 30 }, { 10, 20, 30 }, { 10, 20, 30, }, { 10, 20, 30, } };

    //Level Upgrade
    

    //plus stutus
    public int[] Atkplus = new int[3];
    public int[] HpPlus = new int[3];
    public int[] BagPlus = new int[3];
    public int[] TankPlus = new int[3];

    public GameObject gameObjectATK;
    public GameObject gameObjectHP;
    public GameObject gameObjectBAG;

    //I had
    public int wood;
    public int stone;
    public float ink;
    public int monMat;

    //UI
    public TextMeshProUGUI m_text_AtkCost;
    public TextMeshProUGUI m_text_HpCost;
    public TextMeshProUGUI m_text_BagCost;
    public TextMeshProUGUI m_text_TankCost;
    void Start()
    {
        

        
    }

    // Update is called once per frame
    void Update()
    {
        wood = ResourceMannage.b_wood;
        stone = ResourceMannage.b_stone;
        ink = ResourceMannage.ink;
        monMat = ResourceMannage.b_monMat;





        if (Character_Status_Manager.AtkLevel >= 3)
        {
            m_text_AtkCost.text = "Max";
            gameObjectATK.SetActive(false);

        }else
        {
            m_text_AtkCost.text = "ATKLevel: " + Character_Status_Manager.AtkLevel
                + "\n     Wood Cost: " + AtkCost[0, Character_Status_Manager.AtkLevel] 
                + "\n     Stone Cost: " + AtkCost[1, Character_Status_Manager.AtkLevel] 
                + "\n     Ink Cost: " + AtkCost[2, Character_Status_Manager.AtkLevel]
                + "\n     MonMat Cost: " + AtkCost[3, Character_Status_Manager.AtkLevel];
        }

        if (Character_Status_Manager.HpLevel >= 3)
        {
            m_text_HpCost.text = "Max";
            gameObjectHP.SetActive(false);


        }
        else if(Character_Status_Manager.HpLevel < 3)
        {
            m_text_HpCost.text = "HpLevel: " + Character_Status_Manager.HpLevel
                + "\n     Wood Cost: " + HpCost[0, Character_Status_Manager.HpLevel]
                + "\n     Stone Cost: " + HpCost[1, Character_Status_Manager.HpLevel]
                + "\n     Ink Cost: " + HpCost[2, Character_Status_Manager.HpLevel]
                + "\n     MonMat Cost: " + HpCost[3, Character_Status_Manager.HpLevel];
                
        }

        if (Character_Status_Manager.BagLevel >= 3)
        {
            m_text_BagCost.text = "Max";
            gameObjectBAG.SetActive(false);

        }
        else
        {
            m_text_BagCost.text = "BagCapLevel: " + Character_Status_Manager.BagLevel
                + "\n     Wood Cost: " + BagCapCost[0, Character_Status_Manager.BagLevel] 
                + "\n     Stone Cost: " + BagCapCost[1, Character_Status_Manager.BagLevel] 
                + "\n     Ink Cost: " + BagCapCost[2, Character_Status_Manager.BagLevel]
                + "\n     MonMat Cost: " + BagCapCost[3, Character_Status_Manager.BagLevel]
                ;

        }
        if (Character_Status_Manager.TankLevel >= 3)
        {
            m_text_TankCost.text = "Max";

        }else
        {
            m_text_TankCost.text = "TankLevel: " + Character_Status_Manager.TankLevel
                + "\n     Wood Cost: " + TankCost[0, Character_Status_Manager.TankLevel] 
                + "\n     Stone Cost: " + TankCost[1, Character_Status_Manager.TankLevel] 
                + "\n     Ink Cost: " + TankCost[2, Character_Status_Manager.TankLevel]
                + "\n     MonMat Cost: " + TankCost[3, Character_Status_Manager.TankLevel];
        }




    }
    public void AtkUp()
    {
       

        if((wood >= AtkCost[0, Character_Status_Manager.AtkLevel] && stone >= AtkCost[1, Character_Status_Manager.AtkLevel] && ink >= AtkCost[2, Character_Status_Manager.AtkLevel] && monMat >= AtkCost[3, Character_Status_Manager.AtkLevel]))
        {
            
            if (Character_Status_Manager.AtkLevel >= 3)
            {
                return;

            }else
            {
                ResourceMannage.b_wood -= AtkCost[0, Character_Status_Manager.AtkLevel];
                ResourceMannage.b_stone -= AtkCost[1, Character_Status_Manager.AtkLevel];
                ResourceMannage.ink -= AtkCost[2, Character_Status_Manager.AtkLevel];
                ResourceMannage.b_monMat -= AtkCost[3, Character_Status_Manager.AtkLevel];
                Character_Status_Manager.player_baseDamage += Atkplus[Character_Status_Manager.AtkLevel];
                Character_Status_Manager.AtkLevel++;
            }
        }
        
        //Check Resourse

    }
    public void HpUp()
    {
        if ((wood >= HpCost[0, Character_Status_Manager.HpLevel] && stone >= HpCost[1, Character_Status_Manager.HpLevel] && ink >= HpCost[2, Character_Status_Manager.HpLevel] && monMat >= HpCost[3, Character_Status_Manager.HpLevel]))
        {
            
            if (Character_Status_Manager.HpLevel >= 3)
            {
                
                return;

            }
            else
            {
                ResourceMannage.b_wood -= HpCost[0, Character_Status_Manager.HpLevel];
                ResourceMannage.b_stone -= HpCost[1, Character_Status_Manager.HpLevel];
                ResourceMannage.ink -= HpCost[2, Character_Status_Manager.HpLevel];
                ResourceMannage.b_monMat -= HpCost[3, Character_Status_Manager.HpLevel];
                Character_Status_Manager.player_baseMaxInk += HpPlus[Character_Status_Manager.HpLevel];
                Character_Status_Manager.HpLevel++;
            }
        }
    }
    public void BagCapUp()
    {
        if ((wood >= BagCapCost[0, Character_Status_Manager.BagLevel] && stone >= BagCapCost[1, Character_Status_Manager.BagLevel] && ink >= BagCapCost[2, Character_Status_Manager.BagLevel] && monMat >= BagCapCost[3, Character_Status_Manager.BagLevel]))
        {
            
            if (Character_Status_Manager.BagLevel >= 3)
            {
                return;

            }else
            {
                ResourceMannage.b_wood -= BagCapCost[0, Character_Status_Manager.BagLevel];
                ResourceMannage.b_stone -= BagCapCost[1, Character_Status_Manager.BagLevel];
                ResourceMannage.ink -= BagCapCost[2, Character_Status_Manager.BagLevel];
                ResourceMannage.b_monMat -= BagCapCost[3, Character_Status_Manager.BagLevel];
                Character_Status_Manager.player_baseBag += BagPlus[Character_Status_Manager.BagLevel];
                Character_Status_Manager.BagLevel++;
            }
        }
    }
    public void TankUp()
    {
        if ((wood >= TankCost[0, Character_Status_Manager.TankLevel] && stone >= TankCost[1, Character_Status_Manager.TankLevel] && ink >= TankCost[2, Character_Status_Manager.TankLevel] && monMat >= TankCost[3, Character_Status_Manager.TankLevel]))
        {
            
            if (Character_Status_Manager.TankLevel >= 3)
            {
                return;

            }else
            {
                ResourceMannage.b_wood -= TankCost[0, Character_Status_Manager.TankLevel];
                ResourceMannage.b_stone -= TankCost[1, Character_Status_Manager.TankLevel];
                ResourceMannage.ink -= TankCost[2, Character_Status_Manager.TankLevel];
                ResourceMannage.b_monMat -= TankCost[3, Character_Status_Manager.TankLevel];
                Character_Status_Manager.player_baseMaxInk += HpPlus[Character_Status_Manager.TankLevel];
                Character_Status_Manager.TankLevel++;
            }
        }
    }
}