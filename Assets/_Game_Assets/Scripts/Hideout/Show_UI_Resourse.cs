using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Assets.Game_Assets.Code.Singleton;
using UnityEngine.UI;

public class Show_UI_Resourse : MonoBehaviour
{
    // Start is called before the first frame update
    //UI
    public TextMeshProUGUI m_text_Resourse;
    public TextMeshProUGUI m_text_Day;
    public TextMeshProUGUI m_text_Curse;
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        m_text_Resourse.text = "Wood : " + ResourceMannage.b_wood + "\nStone : " + ResourceMannage.b_stone + "\nMonMaterial : " + ResourceMannage.b_monMat + "\nInk : " + ResourceMannage.ink;

        m_text_Day.text = "Day: " + GameManager.day;

        m_text_Curse.text = "Curse: " + GameManager.curse;
    }
}
