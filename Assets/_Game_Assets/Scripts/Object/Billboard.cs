using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera theCam;

    public bool isBillboard;

    void Start()
    {
        theCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (isBillboard)
        {
            transform.LookAt(theCam.transform);
        }else
        {
            transform.rotation = theCam.transform.rotation;
        }
        transform.rotation = Quaternion.Euler(0f, transform.eulerAngles.y, 0f);
    }
}
