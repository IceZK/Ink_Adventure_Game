using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_animator : MonoBehaviour
{
    public Animator animator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void idle()
    {
        animator.SetBool("isIdle", true);
        animator.SetBool("isWalk", false);
        
    }
    public void Walk()
    {
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalk", true);

    }
    public void Attack()
    {
        animator.SetBool("isIdle", false);
        animator.SetBool("isAttack", true);
    }
}
