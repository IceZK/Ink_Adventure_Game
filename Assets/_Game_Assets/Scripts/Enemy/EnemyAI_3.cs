using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Scprite.Enmey.EnemyAI
{
    public class EnemyAI_3 : MonoBehaviour
    {
        public NavMeshAgent agent;
        Transform Player;
        public SpriteRenderer sr;
        public BoxCollider BoxCollider;
        public Enemy_animator m_animator;
        private void Awake()
        {
            Player = GameObject.FindWithTag("Player").transform;
            BoxCollider = gameObject.GetComponent<BoxCollider>();
            m_animator = GetComponent<Enemy_animator>();
        }
        void Start()
        {

        }
        // Update is called once per frame
        private void Update()
        {
            if (this.transform.position.x > Player.position.x)
            {
                sr.flipX = true;
            }
            else if (this.transform.position.x < Player.position.x)
            {
                sr.flipX = false;
            }
            if(agent.velocity.x == 0 &&  agent.velocity.y == 0 && agent.velocity.z == 0)
            {
                m_animator.idle();
            }
            else if(agent.velocity.x != 0 && agent.velocity.y != 0 && agent.velocity.z != 0)
            {
                m_animator.Walk();
            }
           
        }
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                agent.SetDestination(Player.position);
                
            }
        }
    }
}

