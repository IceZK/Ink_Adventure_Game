﻿using System.Collections;
using UnityEngine;

namespace Assets.Game_Assets.Code.Enemy
{
    public class Field_of_View : MonoBehaviour
    {
        public BoxCollider BoxCollider;
        public GameObject target;
        // Use this for initialization
        void Start()
        {
            BoxCollider = GetComponent<BoxCollider>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject == target)
            {

            }
        }
    }
}