﻿using IceZK_DEV;
using Kagerou;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IceZK_DEV
{
    public class Enmy_Status : MonoBehaviour
    {
        public Enemy_StatusSO StatusSO;
        

        public CombatUI m_CombatUI;
        public FightingSystem fighting;
        public CombatUI combat;
        //base stat

        [Header("base stat")]


        public float baseMaxInk = 0;
        public int baseDamage = 0;
        public float baseInk = 0;
        public int EXP = 0;
        public int monMat = 0;
        public int minDrop = 0;
        public int maxDrop = 0;

        bool TriggerPlayer = false;
        void Start()
        {
            
            m_CombatUI = this.GetComponent<CombatUI>();


            this.gameObject.name = StatusSO.charName;

            baseMaxInk = StatusSO.baseMaxInk;
            baseDamage = StatusSO.baseDamage;
            baseInk = StatusSO.baseInk;
            EXP = StatusSO.EXP;
            monMat = StatusSO.monMat;
            minDrop = StatusSO.minDrop;
            maxDrop = StatusSO.maxDrop;

            monMat = Random.Range(minDrop, maxDrop);
        }
        private void Update()
        {
            GameObject Fighto = GameObject.Find("Combat-Canvas");
            fighting = Fighto.GetComponent<FightingSystem>();
            combat = Fighto.GetComponent<CombatUI>();

            if (fighting.enemy_baseInk <= 0 && TriggerPlayer == true)
            {
                fighting.enemy_baseInk = 1;
                Destroy(this.gameObject);
            }
            if (combat.isEscaped == true && TriggerPlayer == true)
            {
                combat.isEscaped = false;
                Character_Status_Manager.isCombat = false;
                Character_Status_Manager.isPause = false;
                Debug.Log("Destroy");
                Destroy(this.gameObject);
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.tag == "Player" && TriggerPlayer == false)
            {
                Debug.Log("Trigger");

                fighting.enemy_baseMaxInk = this.baseMaxInk;
                fighting.enemy_baseDamage = this.baseDamage;
                fighting.enemy_baseInk = this.baseInk;
                fighting.enemy_EXP = this.EXP;
                fighting.enemy_monMat = this.monMat;
                //valueDrop
                Character_Status_Manager.isCombat = true;
                Debug.Log(Character_Status_Manager.isCombat);
                TriggerPlayer = true;


            }
        }

    }
    
}

    

