using Assets.Game_Assets.Code.Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace IceZK_DEV
{
    [RequireComponent(typeof(NavMeshAIController))]
    public class NavMeshAIDestinationSetter : MonoBehaviour
    {
        [SerializeField] protected NavMeshAIController m_NMAIController;

        [SerializeField] private float surfaceOffset = 0;
        
        public Test_animator _Animator;
        public Player_SFX m_player_SFX;

        public SpriteRenderer sr;
        void Start()
        {
            m_NMAIController = GetComponent<NavMeshAIController>();
            m_player_SFX = GetComponent<Player_SFX>();
            
        }

        // Update is called once per frame
        void Update()
        {
            
            Mouse mouse = Mouse.current;
            if (mouse.leftButton.wasPressedThisFrame)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit) == true)
                {
                    GameObject pickedTarget = new GameObject();
                    pickedTarget.transform.position = hit.point + hit.normal *
                    surfaceOffset;

                    if (this.transform.position.x > pickedTarget.transform.position.x)
                    {
                        sr.flipX = true;
                    }
                    else if (this.transform.position.x < pickedTarget.transform.position.x)
                    {
                        sr.flipX = false;
                    }
                    //this.transform.position.x != pickedTarget.transform.position.x && this.transform.position.z != pickedTarget.transform.position.z && 
                    if (this.transform.position.x != pickedTarget.transform.position.x && this.transform.position.z != pickedTarget.transform.position.z)
                    {
                        _Animator.Walk();
                        if (Character_Status_Manager.isCombat == false && Time.timeScale == 1)
                        {
                            m_player_SFX.WalkSFX();
                        }
                       

                        
                    }
                    
                    

                    m_NMAIController.SetTarget(pickedTarget.transform);

                    Destroy(pickedTarget, 0.2f);

                }
            }
        }
        
    }
        
}

