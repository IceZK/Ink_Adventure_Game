using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IceZK_DEV
{
    public enum ItemType {Material, HugeMaterial}
    public enum MatType {Wood, Stone, Mon}
    [CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item", order = 1)]

    public class ItemSO : ScriptableObject
    {
        protected ItemType m_ItemType;
        public string ItemName;
        public Sprite icon;
        public ItemType type;
        public MatType mattype;
        public int amount;
        public int HugeAmount = 1;


        
        
        

    }
}

