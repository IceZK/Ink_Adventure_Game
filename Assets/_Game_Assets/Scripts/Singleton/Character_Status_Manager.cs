﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace IceZK_DEV
{
    public class Character_Status_Manager : MonoBehaviour
    {
        public static Character_Status_Manager CSM_instance;

        
        //base stat
        [Header("Player stat")]

        public static float player_baseMaxCurse = 100;
        public static float player_baseMaxInk = 100;
        public static int player_baseBagCap = 10;


        public static int player_baseDamage = 10;
        public static float player_baseCurse = 0;
        public static float player_baseInk = 100;
        public static float player_InkTemp = 0;
        public static float player_inkTempCap = 0;
        public static int player_baseBag = 0;

        public static int player_EXP = 0;
        public static int player_LV = 0;

        public static int AtkLevel = 0;
        public static int HpLevel = 0;
        public static int BagLevel = 0;
        public static int TankLevel = 0;

        [Header("Enemy stat")]
        public static float enemy_baseMaxInk = 20;
        public static int enemy_baseDamage = 10;
        public static float enemy_baseInk = 20;
        public static int enemy_EXP = 10;
        public static int enemy_monMat = 10;
        // Use this for initialization

        //CombatUI
        public static bool isCombat = false;
        public static bool isPause = false;
        private void Awake()
        {
            // if the singleton hasn't been initialized yet
            if (CSM_instance != null && CSM_instance != this)
            {
                Destroy(this.gameObject);

            }
            else
            {
                DontDestroyOnLoad(this.gameObject);
                CSM_instance = this;


            }
        }
        
        public void Fill_ink()
        {
            player_baseInk = player_baseMaxInk;
        }
        
        

    }
}