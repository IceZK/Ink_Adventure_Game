using IceZK_DEV;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceMannage : MonoBehaviour
{
    
    public static ResourceMannage Resouurce_instance;
    public static int i_wood;
    public static int i_stone;
    public static int i_monMat;
    public static int b_wood;
    public static int b_stone;
    public static int b_monMat;
    public static float ink = 1000;
    public static int day;
    public static float curse;
	// Start is called before the first frame update

	 private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (Resouurce_instance != null && Resouurce_instance != this)
        {
            Destroy(this.gameObject);
            
        }
        else 
        {
            DontDestroyOnLoad(this.gameObject);
            Resouurce_instance = this;
            
            
        }
    }
    

    public void AddItem(MatType matType, int amount)
    {
        switch (matType)
        {
            case MatType.Wood: 
                i_wood += amount; break;
            case MatType.Stone: 
                i_stone += amount; break;
            case MatType.Mon:
                i_monMat += amount; break;
                default: return;
                
        }
    }
    

    public void Gohome()
    {
        b_wood += i_wood;
        b_stone = i_stone;
        b_monMat += i_monMat;
    }
    public void ResetResourse()
    {
        i_wood = 0;
        i_stone = 0;
        i_monMat = 0;
    }

}
